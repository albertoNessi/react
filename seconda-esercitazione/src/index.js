/**
 * importare il file delle transazioni
 * 1. creare una funzione che data una mail e le transazioni, ritorna l'array delle transazioni di quell'utente
 * 2. creare una funzione che date le transazioni, ritorna il totale dei kwh erogati
 * 3. creare una funzione che date le transazioni, ritorna soltanto le transazioni che sono durate più di 0
 * 4. creare una funzione che date le transazioni, per ognuna di esse ritorna il nome di colui che ha fatto la ricarica
 */

// 1.

//standard
import _ from "lodash";

import transactions from "./transactions.json";

const filterTransactionsByEmail = (transactions, email) => {
  return transactions.filter((t) => {
    // t è l'i-esimo, nella fattispecie è una transaction
    const transactionEmail = _.get(t, "invoicedTo.email");
    return transactionEmail === email;
  });
};

const filterdTransactions = filterTransactionsByEmail(
  transactions,
  "oltion.abazi@gmail.com"
);
console.log(filterdTransactions);

// 2.1

//const getTotalKwh = (transactions) => {
//  return _.reduce(transactions, (acc, t) => {
//    const rechargeKwh = _.get(t, 'rechargeKwh', 0)
//    return acc += rechargeKwh
//  }, 0)
//}

const getTotalKwh = (transactions) => {
  return transactions.reduce((acc, t) => {
    const rechargeKwh = _.get(t, "rechargeKwh", 0);
    return (acc += rechargeKwh);
  }, 0);
};

const myKwh = getTotalKwh(transactions);
console.log(myKwh);

// 2. bis esempio di get

const myObj = {
  a: {
    aa: "1"
  },
  c: {
    aa: 1
  }
};

//console.log(_.get(myObj, 'b.aa', 'non trovato'))
//console.log(myObj.b.aa)

// 3.

const getValidTransaction = (transactions, threshold) => {
  return transactions.filter((t) => t.rechargeDurationSeconds > 0);
};

const validTransaction = getValidTransaction(transactions);
console.log(validTransaction);

//4.

const getTransactionUsers = (transactions) => {
  return transactions.map((t) => _.get(t, "invoicedTo.firstname"));
};

const users = getTransactionUsers(transactions);
console.log(users);

// 5.
/**
 * groupBy ci costruisce un oggetto (a partire da un array)
 * in cui decidiamo noi per cosa raggruppare i vari elementi (secondo una fuznione)
 *
 */

const groupedTransactions = _.groupBy(transactions, (t) => {
  return _.get(t, "rechargeKwh");
});

console.log(groupedTransactions);
