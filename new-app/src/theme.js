const LIGHT_BLUE = "#007fff";

export default {
  darkBlue: "#0000AF",
  lightBlue: LIGHT_BLUE
};
