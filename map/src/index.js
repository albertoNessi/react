import transactions from "./transactions.json";
import _ from "lodash";
console.log(transactions);

const brokenTransactions = null;

const mapTransaction = () => {};
//.map
transactions.map(() => {});

const numbers = [1, 3, 5, 7];

const doubledNumbers = numbers.map((n) => {
  return n * 2;
});

console.log(doubledNumbers);
console.log(numbers);
/* 1° iterazione, la funzione dentro avrà n = 1 doubledNumbers = [2]
 *  2° iterazione, la funzione avrà ingresso n = 3, doubledNUmbers = [2,6]
 *
 *
 */
//.filter
