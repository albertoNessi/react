// 1. MAP input array / object => output array
// entra array di N => esce array di N
// 2. FILTER input array / object => output array
// entra array di N => esce array di dimensioni tra 0 e N
// 3. REDUCE parti da un collection (array/object) => ritorna un singolo elemento

// NB: se ometti l'estensione, è da considerarsi javascript
// SE Oometto il file, cerca index.js, vedi index.js dentro nested-folder
import myModule from "./nested-folder";
import chargers from "./chargers.json";

const numbers = [1, 2, 3, 4, 5];

const oddNumbers = numbers.filter((n) => n % 2 !== 0);
console.log(oddNumbers);

// funzione
// l'ho definita
// !!!!!!11!!1! NON LA STO USANDO, L'HO SOLO DEFINITA;
const getMyCharger = (chargers, chargerId) => {
  console.log(chargerId);
  return chargers.find((c) => c.id === chargerId);
};

// l'ho INVOCATA, L'HO CHIAMATA!!!!!!1111|!|\
// quando le funzioni ritornano qualcosa e io voglio quel qualcosa
// faccio const variable = myfunc
// L'ORDINE E' IMPORTANTE
const myCharger = getMyCharger(chargers, "5fd978996550a72329e49511");
// ho omesso di passare l'id, quando la funzione verrà eseguita, chargerId = undefined
const myChargerNoId = getMyCharger(chargers); 
console.log(myChargerNoId);

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
// default parameter
const returnMyNumber = (n, m = 3) => {
  return n + m
}

// equivalente seza default parameter (GUARDATEVI BENE IL LINK)
const returnMyNumberWithParaculo = (n, m) => {
  if (m === undefined) m = 3;
  return n + m;
}

const myNumberTwo = returnMyNumber(2, 4);
const myNumber = returnMyNumber(2);
