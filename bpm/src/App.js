import "./styles.css";
import React, { useState } from "react";

export default function App() {
  const [counter, setCounter] = useState([]);

  const updateCounter = () => {
    setCounter([...counter, { time: new Date() }]);
  };

  const getBpm = () => {
    if (counter.length > 1) {
      const initialTime = counter[0].time.getTime();
      const finalTime = counter[counter.length - 1].time.getTime();
      return (counter.length / (finalTime - initialTime)) * 60000;
    }
    return 0;
  };

  const bpm = getBpm();

  const reset = () => {
    setCounter([]);
  };

  return (
    <div className="App">
      <div className="counter">{Math.round(bpm)}</div>
      {/* <div>Contatore: {counter.length}</div> */}
      <div id="divCircles">
        {counter.map((element, index) => {
          if (index % 4 === 0) {
            return (
              <div className="circles" style={{ marginLeft: "20px" }}></div>
            );
          }
          return <div className="circles"></div>;
        })}
        {console.log(counter.map)}
      </div>
      <div className="button" onClick={updateCounter}>
        <p>bpm</p>
      </div>
      {/* <div>{dpm}</div> */}
      {/* {counter.map((element) => {
        return <div>{element.time.getTime()}</div>;
      })} */}
      <div id="resetDiv" onClick={reset}>
        Reset
      </div>
    </div>
  );
}
