import "./styles.css";
import React, { useState, useEffect } from "react";
import { AiOutlineDelete } from "react-icons/ai";

export default App = () => {
  const [title, setTitle] = useState("");
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    console.log("effect");
    apiCall();
    // getFilteredList();
  }, []);

  const apiCall = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts");
    const json = await response.json();
    setIsLoading(false);
    setList(json);
  };

  const handleChange = (event) => {
    console.log(event.target.value);
    setTitle(event.target.value);
    // setFilter(event.target.value);
    event.preventDefault();
  };

  // const getFilteredList = () => {
  //   if (!filter) {
  //     return list;
  //   } else {
  //     const newList = list.filter((listElement) => {
  //       listElement.title.includes(filter);
  //     });
  //     return newList;
  //   }
  // };

  // const filteredList = getFilteredList();

  const addToList = (element) => {
    setTitle("");
    if (title === "" || list.find((element) => element.title === title)) {
      alert("Hai già inserito questo valore!");
    } else {
      setList([...list, { title, id: Math.random() }]);
    }
  };
  const deleteElementToList = (selectedElement) => {
    const selectedIndex = list.findIndex(
      (element) => element.id === selectedElement.id
    );
    // console.log(selectedIndex);
    list.splice(selectedIndex, 1);
    setList([...list]);
    setTitle("");
  };

  // console.log(list);
  return (
    <div className="App">
      <div className="header">
        <p>MyToDoList</p>
        <div id="inputContainer">
          <input
            type="text"
            id="inputText"
            placeholder="Search"
            value={title}
            onChange={handleChange}
          />
          <div id="inputAdd" onClick={(element) => addToList(element)}>
            Add
          </div>{" "}
        </div>
        {/* <input type="submit" value="Add" id="inputAdd" onClick={addToList} /> */}
      </div>
      <div>
        {isLoading && <div className="loader"></div>}
        {list.map((element, index) => {
          return (
            <div id="divList" key={element.id}>
              <div id="divDescrizione">
                <p className="description">{element.title}</p>
              </div>
              <p
                className="deleteLogo"
                onClick={() => deleteElementToList(element)}
              >
                <AiOutlineDelete />
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
};
