import { StrictMode } from "react";
import ReactDOM from "react-dom";
import Rhombus from "./Rhombus";
import FullName from "./FullName";
// import Console from "./Console";
import Title from "./Title";
import DateOfCreation from "./DateOfCreation";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <StrictMode>
    <Title />
    <DateOfCreation />
    <Rhombus />
    <FullName firstName="Alberto" lastName="Nessi" />
    {/* <Console /> */}
  </StrictMode>,
  rootElement
);
