const DateOfCreation = () => {
  const Months = new Array(12);
  Months[0] = "January";
  Months[1] = "February";
  Months[2] = "March";
  Months[3] = "April";
  Months[4] = "May";
  Months[5] = "June";
  Months[6] = "July";
  Months[7] = "August";
  Months[8] = "September";
  Months[9] = "October";
  Months[10] = "November";
  Months[11] = "December";

  const thisMonth = Months[new Date().getMonth()];

  const Week = new Array(7);
  Week[0] = "Sunday";
  Week[1] = "Monday";
  Week[2] = "Tuesday";
  Week[3] = "Wednesday";
  Week[4] = "Thursday";
  Week[5] = "Friday";
  Week[6] = "Saturday";

  const DayOfTheWeek = Week[new Date().getDay()];

  const NumberOfTheDay = new Date().getDate();

  return (
    <div>
      <p class="subTitle">
        Created on: {DayOfTheWeek}, {thisMonth} {NumberOfTheDay},{" "}
        {new Date().getFullYear()}
      </p>
    </div>
  );
};

export default DateOfCreation;
