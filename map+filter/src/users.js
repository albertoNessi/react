import users from "./users.json";
// non sto usando nessun path perché lodash è installato come libreria da npm
import _ from "lodash";
// array = BIDIMENSIONALE sempre foreach o filter

// il metodo map, vuole un parametro che è una funzione
// con un solo argomento che rappresenta l'i-esima
// elemento del array
const shortenedUsers = users.map((user) => {
  return user.firstname + " " + user.lastname;
});

const shortendUsersAsObj = users.map((user) => {
  const newUser = {
    nome: user.firstname,
    cognome: user.lastname
  };
  return newUser;
});

const array = [[1, 2], [3, 4], 3];
const numbers = [1, 2, 3, 4];

// users è bidimensionale (array o object)
// user dentro la funzione, è l'iesimo elemento
const coolUsers = _.map(users, (user) =>
  _.pick(user, ["firstname", "lastname"])
);

console.log(users);
// versione figa
const confirmedUsers = users.filter((user) => user.isConfirmed);

const usersWithMinutes = users.filter((user) => {
  return user.bonusMinutes > 0;
});
console.log(usersWithMinutes);

// il metodo MAP vuole come parametro una funzione.
// questa funzione che do in ingresso a map, è una funzione con un unico argomento
// questo arogmento è l'i-esimo elemento dell'array
// che ritorna qualcosa
// array.map => ritorna un altro array delle stesse dimensioni

const multiplyNumber = (numbers, multiplier) => {
  if (!numbers) return [];
  const multipliedNumber = numbers.map((n) => {
    return n * multiplier;
  });
  return multipliedNumber;
};

const myNumbers = multiplyNumber(numbers, 3);
console.log(myNumbers);

// 1. scrivo .map() sull'array che voglio CICLARE
// 2. dentro map, come unico argomento: scrivo una fuznione () => {}
// 3. come unico argomento della funzione dentro map, scriv argomento
// che rappresenta l'iesimo elemento dell'array.
// 4. assicuriamoci che la funzione dentro map RITORNI QUALCOSA

const divivedNumbersByThree = numbers.map((number) => number / 3);
console.log(divivedNumbersByThree);
