/**
* scrivere una funzione che ha come argomento un oggetto.
* questa funzione sostituisce i valori vuoti dell'oggetto (STRINGA VUOTA)
* con null.
* ESEMPIO
* { a: '', b: '2', c: ''} ------> { a: null, b: '2', c: null}
*/

/**
* 
Write a function that takes two objects as arguments
// Unfortunately, the property 'country' in the second object has the wrong key
// It should be named 'city' instead
// Merge both objects and correct the wrong property name
// Return the resulting object
// It should have the properties 'planet', 'continent', 'country', 'state', and 'city'
/**
* 
myFunction({ continent: 'Europe', country: 'Germany' }, { planet: 'Earth', country: 'Munich', state: 'Bavaria' })
Expected
{ continent: 'Europe', country: 'Germany', planet: 'Earth', state: 'Bavaria', city: 'Munich'} * 
*/

/**
*
* creare una funzione che dato in ingresso una stringa,
* ritorna una oggetto che ha come chiave i caratteri della stringa e come valore
* la ricorrenza di questi caratteri
* ESEMPIO:
* 'aiuola' ---> { a: 2, i: 1, u: 1, o: 1, l: 1}
* 'mamma' ----> { m: 3, a: 2}
*/