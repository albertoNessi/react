/**
Write a function that takes two objects as arguments
// Unfortunately, the property 'country' in the second object has the wrong key
// It should be named 'city' instead
// Merge both objects and correct the wrong property name
// Return the resulting object
// It should have the properties 'planet', 'continent', 'country', 'state', and 'city'

myFunction({ continent: 'Europe', country: 'Germany' }, { planet: 'Earth', country: 'Munich', state: 'Bavaria' })
Expected
{ continent: 'Europe', country: 'Germany', planet: 'Earth', state: 'Bavaria', city: 'Munich'} * 
*/
import _ from "lodash";
const milan = {
  planet: "Earth",
  continent: "Europe",
  country: "Italy",
  state: "Lombardia",
  city: "Milan"
};

const milanWrong = {
  planet: "Earth",
  continent: "Europe",
  country: "Milan",
  state: "Lombardia"
};
let correctMilanObj;

const correctMilan = () => {
  milanWrong.country = milanWrong.city;
  correctMilanObj = _.merge(milan, milanWrong);
};

correctMilan(milan, milanWrong);
console.log(correctMilanObj);
