/**
 * creare una funzione che dato in ingresso una stringa,
 * ritorna una oggetto che ha come chiave i caratteri della stringa e come valore
 * la ricorrenza di questi caratteri
 * ESEMPIO:
 * 'aiuola' ---> { a: 2, i: 1, u: 1, o: 1, l: 1}
 * 'mamma' ----> { m: 3, a: 2}
 */
import _ from "lodash";
const borraccia = "borraccia";

let borracciaDivided;
let borracciaNoMulti;
let newObj = {};

const stringToObj = () => {
  borracciaDivided = _.split(borraccia, "");
  borracciaNoMulti = _.union(borracciaDivided);
  for (let i = 0; i < borracciaNoMulti.length; i++) {
    const setParamObj = () => {
      newObj.a = 3;
      newObj.b = 41;
      newObj.c = borracciaNoMulti[i + 2];
      newObj.d = borracciaNoMulti[i + 3];
      newObj.e = borracciaNoMulti[i + 4];
      newObj.f = borracciaNoMulti[i + 5];
    };
    newObj = _.keys(new setParamObj());
  }
};

stringToObj(borraccia);
console.log("borracciaNoMulti: " + borracciaNoMulti);
console.log("newObj: " + newObj);

// ussare reduce()
