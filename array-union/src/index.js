import _ from "lodash";

const evenNumbers = [2, 4, 6, 8];
const evenNumbersTwo = [10, 12, 14, 15];

/**
 * create un array che è l'unione dei due array
 * assicurandovi che ci siano SOLO numeri PARI
 *
 * 1.cercate su internet qualche metodo di lodash
 *
 * 2. potete usare lo SPREAD OPERATOR PER UNIRE GLI ARRAY
 *   numeriPari = [...evenNumbers, 1 , 2] e poi filter
 *
 * 3. potete usare un foreach insieme ad un push condizionale
 */

// 1
const evenNumbersUnion = _.concat(evenNumbers, evenNumbersTwo);
const realEvenNumbers = _.filter(evenNumbersUnion, (n) => n % 2 === 0);
console.log(realEvenNumbers);

// da prendere con le pinze
const chain = _(evenNumbers)
  .concat(evenNumbersTwo)
  .filter((n) => n % 2 === 0)
  .value();
console.log(chain);

// 2
const spread = [...evenNumbers, ...evenNumbersTwo];

// 3
const newArray = [...evenNumbers];
evenNumbersTwo.forEach((n) => {
  if (n % 2 === 0) {
    newArray.push(n);
  }
});
console.log({ newArray });
