import chargers from "./chargers.json";

/**
 * NB. NON STO PARLANDO DELLE FUNZIONI DENTRO
 * map => 1 ARGOMENTO
 * filter => 1 ARGOMENTO
 * find => 1 ARGOMENTO
 * reduce => 2 ARGOMENTO: reducer e initialValue
 */

// reduce
/**
 * lo eseguo su un array,
 * REDUCE VUOLE 2 ARGOMENTI: UN REDUCER E UN VALORE INIZIALE
 * vuole in pasto una funzione che è un reducer E UN VALORE INIZIALE
 * questo reducer ha FINO a 4 argomenti il orimo è l'accumulatore, il secondo è il valore corrente
 * il terzo è l'indici
 * REDUCE VUOLE CHE TORNIATE ACC AD OGNI ITERAZIONE
 * ritorna un UNICO valore!!1111!11!
 */

const numbers = [12, 34, 22, 78, 45, 1];

const reducer = (acc, cur, idx, src) => {
  const newValue = acc + cur;
  console.log(newValue);
  return newValue;
};

const sum = numbers.reduce(reducer, 0);

console.log(sum);
