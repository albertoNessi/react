import _ from "lodash";

export const capitalize = (word) => {
  return _.capitalize(word);
};

// tutte e 3 equivalenti

export const mapVarToObj = (var1, var2) => {
  const obj = {
    var1,
    var2
  };
  return obj;
};

export const mapVarToObj2 = (var1, var2) => {
  return {
    var1,
    var2
  };
};

export const mapVarToObj3 = (var1, var2) => ({ var1, var2 });
