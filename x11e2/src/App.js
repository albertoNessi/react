import "./styles.css";
import Circle from "./Circle";
import Rhombus from "./Rhombus";
import FullName from "./FullName";
import FullNameTwo from "./FullNameTwo";

const App = () => {
  const isVisible = true;
  return (
    <div className="App">
      <Circle />
      <Rhombus />
      <FullName isVisible={isVisible} firstName={"banana"} lastName={"split"} />
      <FullNameTwo
        isVisible={!isVisible}
        firstName={"Vedi"}
        lastName={"nel parchetto"}
      />
    </div>
  );
}
export default App;
